package com.hui.bean;

import lombok.Data;

/**
 * Created by yhq on 17-6-4.
 */
@Data
public class Person {

    private String name;
    private Integer age;

}
