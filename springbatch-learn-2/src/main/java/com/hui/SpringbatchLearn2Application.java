package com.hui;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbatchLearn2Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringbatchLearn2Application.class, args);
	}
}
