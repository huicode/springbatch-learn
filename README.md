#SpringBatch-learn

示例代码地址：https://git.oschina.net/huicode/springbatch-learn

项目章节说明：


- learn1: quick start 和 springBatch的基本架构
- learn2: flatfile(XML,CSV,TXT) 文件的读写
- learn3: 数据库data的迁移（JdbcTemplate）
- learn4: 数据库游标方式，分页方式读写 （JdbcTemplate）
- learn5: springBatch and mybatis
- learn6: listener
- learn7: springBatch skip,retry,transaction
- learn8: JobParameters
- learn9: JobRepository,JobLauncher,JobExplorer
- learn10: springBatch and scheduling
- learn11: springBatch admin
- learn12: 高可用springBatch
- learn13: quick start,springCloudTask 架构
- learn14: springCloudTask and springBatch
- learn15: springCloudTask and springBatch and stream(Kafka)
- ... 待定