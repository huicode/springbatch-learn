package com.hui;

import com.hui.config.BatchConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@EnableBatchProcessing
@Slf4j
public class SpringbatchLearn3ApplicationTests {

	private BatchConfiguration batchConfiguration;

	@Test
	public void contextLoads() {
		log.info("com.hui.SpringbatchLearn3ApplicationTests.contextLoads");
	}

}
